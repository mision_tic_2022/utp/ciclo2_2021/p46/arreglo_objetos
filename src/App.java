public class App {
    public static void main(String[] args) throws Exception {
        
        //Arreglo de objetos
        Persona[] persona = new Persona[2];
        //Construimos los objetos
        persona[0] = new Persona();
        persona[1] = new Persona("Juan", "Hernandez", "123456");

        System.out.println("Nombre de persona[0]: "+persona[0]);
        System.out.println("persona[1]-> "+persona[1]);


    }
}
